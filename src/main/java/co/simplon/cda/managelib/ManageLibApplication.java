package co.simplon.cda.managelib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManageLibApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManageLibApplication.class, args);
	}

}
